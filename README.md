## gitlab静态网页布署[ReadMe]( https://gitlab.com/xuyq123/plain )

### gitlab_pages

| 序号   | 链接                                                                                                                            | 
| -----  | --------------------------------------------------------------------------------------------------------------------------      | 
| 1      | [首页]( https://xuyq123.gitlab.io/plain/index.html )                                                                            |
| 2      | [书法练习轨迹--明月几时有]( https://xuyq123.gitlab.io/plain/%E4%B9%A6%E6%B3%95%E7%BB%83%E4%B9%A0%E8%BD%A8%E8%BF%B9--%E6%98%8E%E6%9C%88%E5%87%A0%E6%97%B6%E6%9C%89.html )     |
| 3      | [笔名汉字频率分析]( https://xuyq123.gitlab.io/plain/%E7%AC%94%E5%90%8D%E6%B1%89%E5%AD%97%E9%A2%91%E7%8E%87%E5%88%86%E6%9E%90 )  | 
| 4      | [书法练习轨迹ReadMe]( https://xuyq123.gitlab.io/plain/%E4%B9%A6%E6%B3%95%E7%BB%83%E4%B9%A0%E8%BD%A8%E8%BF%B9ReadMe.html )     |
	  
```
首页  https://xuyq123.gitlab.io/plain/index.html                           
                                              
书法练习轨迹--明月几时有  https://xuyq123.gitlab.io/plain/%E4%B9%A6%E6%B3%95%E7%BB%83%E4%B9%A0%E8%BD%A8%E8%BF%B9--%E6%98%8E%E6%9C%88%E5%87%A0%E6%97%B6%E6%9C%89.html 

笔名汉字频率分析  https://xuyq123.gitlab.io/plain/%E7%AC%94%E5%90%8D%E6%B1%89%E5%AD%97%E9%A2%91%E7%8E%87%E5%88%86%E6%9E%90 

书法练习轨迹ReadMe   https://xuyq123.gitlab.io/plain/%E4%B9%A6%E6%B3%95%E7%BB%83%E4%B9%A0%E8%BD%A8%E8%BF%B9ReadMe.html

```

### 布署方法

```
gitlab pages   
gitlab布署网页与gitee、github不一样，无需启动。只要添加文件即可。
1、添加  .gitlab-ci.yml 文件 。
2、创建public文件夹，并将html文件上传到此处。
3、推送文件。在gitlab项目 CI/CD --> Jobs 查看是否布署成功。
4、访问页面。如 ：  https://xuyq123.gitlab.io/plain/index.html

参考项目： https://gitlab.com/xuyq123/plain

--- 

.gitlab-ci.yml 文件

pages:
  stage: deploy
  script:
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
  only:
  - master
   
```